#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
  vector<vector<int>> threeSum(vector<int> &nums) {
    if (nums.size() < 3)
      return {};
    std::vector<vector<int>> res;
    std::unordered_multimap<int, int> hashmap;
    for (int i = 0; auto const &iter : nums) {
      hashmap.insert(make_pair(iter, i));
      ++i;
    }
    for (int i = 0; i < nums.size(); ++i) {
      for (int k = i + 1; k < nums.size(); ++k) {
        auto [begin, end] = hashmap.equal_range((nums[i] + nums[k]) * -1);
        for(; begin != end; ++begin){
            auto const &[key, val] = *begin;
            if(val == i || val == k) continue;
            vector toPush = {nums[i], nums[k], key};
            std::sort(toPush.begin(), toPush.end());
            res.push_back(toPush);
        }
      }
    }
    std::sort(res.begin(), res.end());
    res.erase(std::unique(res.begin(), res.end()), res.end());
    return res;
  }
};
