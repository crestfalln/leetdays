#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
  void genparen_impl(int n, int stack,
                     std::vector<std::string> &res, std::string &mod) {
    if (n == mod.size()) {
      res.push_back(mod);
      return;
    }
    if (stack != 0) {
      mod[n] = ')';
      genparen_impl(n + 1, stack - 1, res, mod);
    }
    if (stack < (mod.size() - n)) {
      mod[n] = '(';
      genparen_impl(n + 1, stack + 1, res, mod);
    }
  }
  vector<string> generateParenthesis(int n) {
      std::string mod(n*2, 'X');
      std::vector<std::string> res;
      genparen_impl(0, {}, res, mod);
      return res;
  }
};

int main(){
    Solution sol;
    for(auto iter : sol.generateParenthesis(8)){
        std::cout << iter << " ";
    }
}
