#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
  struct pos {
    uint64_t i, j, max;
  };
  std::vector<std::vector<int>> mem;
  int lp_impl(std::string const &s, int x, int y) {
    if (mem[x][y] != -1)
      return mem[x][y];
    if (x == y) {
      mem[x][y] = 1;
      return 1;
    }
    mem[x][y] = s[x] == s[y]
                    ? mem[std::min(x + 1, static_cast<int>(s.size()) - 1)]
                         [std::min(y - 1, 0)] +
                          1
                    : 0;
    return mem[x][y];
  }
  string longestPalindrome(string s) {
      mem = std::vector<std::vector<int>>(s.size(), std::vector<int>(s.size(), -1));
      int resI = 0, resJ = 0, max = 0;
      for(int i = 0; i < s.size(); ++i){
          for(int j = i; j < s.size(); ++j){
          }
      }
  }
};
