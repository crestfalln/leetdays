#include <bits/stdc++.h>
using namespace std;

// class Solution {
// public:
//     const std::unordered_map<char, uint64_t> literal_vals = {
//         {'I', 1},
//         {'V', 5},
//         {'X', 10},
//         {'L', 50},
//         {'C', 100},
//         {'D', 500},
//         {'M', 1000},
//     };
//     int romanToInt(std::string s) {
//         int res = 0;
//         int max = 1;
//         for(auto const & iter : s | std::views::reverse) {
//             int val = literal_vals.at(iter);
//             if(val < max) val *= -1;
//             res += val;
//             max = max < val ? val : max;
//         }
//         return res;
//     }
// };
//
class Solution {
public:
    const std::unordered_map<char, uint64_t> literal_vals = {
        {'I', 1},
        {'V', 5},
        {'X', 10},
        {'L', 50},
        {'C', 100},
        {'D', 500},
        {'M', 1000},
    };
    int romanToInt(std::string s) {
        int res = 0;
        int max = 1;
        for(auto iter = s.rbegin(); iter != s.rend(); ++iter) {
            int val = literal_vals.at(*iter);
            if(val < max) val *= -1;
            res += val;
            max = max < val ? val : max;
        }
        return res;
    }
};

int main (){
    Solution sol;
    std::cout << sol.romanToInt("MCMXCIV");
}
