#include <bits/stdc++.h>

class Solution {
    const std::string signed_max =
        std::to_string(std::numeric_limits<int32_t>::max());
    const std::string signed_min =
        std::to_string(std::numeric_limits<int32_t>::min());
public:
  int reverse(int x) {
    std::string max = signed_max;
    std::string tmp = std::to_string(x);
    std::string rev;

    if(x < 0) {
        tmp = tmp.substr(1, tmp.size() - 1);
        rev.push_back('-');
        max = signed_min;
    }
    rev.insert(rev.end(), tmp.rbegin(), tmp.rend());

    if(tmp.size() != 10) return std::stoi(rev);
    return std::lexicographical_compare(rev.begin(), rev.end(), max.begin(), max.end()) ? stoi(rev) : 0;
  }
};

int main() {
  Solution sol;
  std::cout << sol.reverse(2122625301);
}
