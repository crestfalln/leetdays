#include <bits/stdc++.h>
using namespace std;

class Solution {
  const std::unordered_map<int, std::string> literal_values = {
      {2, "abc"}, {3, "def"},  {4, "ghi"}, {5, "jkl"},
      {6, "mno"}, {7, "pqrs"}, {8, "tuv"}, {9, "wxyz"}};

public:
  void combinations_impl(std::string const &digits, int n,
                         std::vector<std::string> &result, std::string &mod) {
    if (n == digits.size()) {
      result.push_back(mod);
      return;
    }
    std::string literals = literal_values.at(digits[n] - '0');
    for (auto const &iter : literals) {
      mod[n] = iter;
      combinations_impl(digits, n + 1, result, mod);
    }
  }
  vector<string> letterCombinations(string digits) {
    if (digits.size() == 0)
      return {};
    std::vector<std::string> result;
    std::string mod = digits;
    combinations_impl(digits, 0, result, mod);
    return result;
  }
};

int main() {
  Solution sol;
  for (auto const &iter : sol.letterCombinations("23")) {
    std::cout << iter << " ";
  }
}
