#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
  string longestCommonPrefix(vector<string> &strs) {
    if(strs.size() == 0) return 0;
    int i = std::min_element(strs.begin(), strs.end(),
                             [](std::string const &a, std::string const &b) {
                               return a.size() < b.size();
                             })
                ->size();
    int j = 0;
    for (j = 0; j < i; ++j) {
      char c = strs.front()[j];
      if (!std::all_of(
              strs.begin() + 1, strs.end(),
              [&j, &c](std::string const &str) { return str[j] == c; }))
        break;
    }
    return strs.front().substr(0, j);
  }
};
