#include <bits/stdc++.h>
using namespace std;

struct ListNode {
  int val;
  ListNode *next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
  ListNode *removeNthFromEnd(ListNode *head, int n) {
    ListNode *iter = head;
    int size = 0;
    while (iter != nullptr) {
      ++size;
      iter = iter->next;
    }
    int nFromFront = size - n;
    auto newHead = ListNode(0, head);
    iter = &newHead;
    for(int i = 0; i < nFromFront; ++i)
       iter = iter->next;
    auto toRem = iter->next;
    iter->next = toRem->next;
    delete toRem;
    return newHead.next;
  }
};
