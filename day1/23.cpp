#include <bits/stdc++.h>
using namespace std;

struct ListNode {
  int val;
  ListNode *next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
  ListNode *mergeKLists(vector<ListNode *> &lists) {
    ListNode fakeHead = ListNode();
    ListNode *list3 = &fakeHead;
    auto last =
        std::remove_if(lists.begin(), lists.end(),
                       [](auto const &node) { return node == nullptr; });
    while (lists.begin() != last) {
      auto min = std::min_element(lists.begin(), last,
                                  [](auto const &node1, auto const &node2) {
                                    return node1->val < node2->val;
                                  });
      list3->next = *min;
      *min = (*min)->next;
      list3 = list3->next;
      if (*min == nullptr) {
        --last;
        std::swap(*min, *last);
      }
    }
    return fakeHead.next;
  }
};
