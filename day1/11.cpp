#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
  int maxArea(vector<int> &height) {
    int i = 0, j = height.size() - 1;
    int max = 0;
    while (i < j) {
      int area = (j - i) * std::min(height[j], height[i]);
      max = max < area ? area : max;
      auto cpyI = i, cpyJ = j;
      if (height[cpyI] <= height[cpyJ])
        ++i;
      if (height[cpyJ] <= height[cpyI])
        --j;
    }
    return max;
  }
};

int main(){
    std::vector heights = {1};
    Solution sol;
    std::cout << sol.maxArea(heights);
}
