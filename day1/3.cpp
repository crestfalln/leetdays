#include <string>
#include <unordered_set>
#include <iostream>

using namespace std;

class Solution {
public:
  auto lengthOfLongestSubstring(string s) {
    std::unordered_set<char> set;
    uint64_t i = 0, j = 0;
    uint64_t resI = i, resJ = j;
    uint64_t max = 0;
    while (i < s.size() && j < s.size()) {
      while (set.find(s[i]) == set.end() && i < s.size()) {
        set.insert(s[i]);
        ++i;
      }
      if (max < (i - j)) {
        max = i - j;
        resI = i;
        resJ = j;
      }
      while (set.find(s[i]) != set.end() && i < s.size()) {
        set.erase(s[j]);
        ++j;
      }
    }
    //return std::pair(max, s.substr(resJ, max));
    return max;
  }
};

int main(){
   Solution sol;
   // auto [first, second] = sol.lengthOfLongestSubstring("abbcaxb");
   // std::cout << first << " " << second;
}
